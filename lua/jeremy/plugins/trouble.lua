return {
	'folke/trouble.nvim',
	dependencies = { 'nvim-tree/nvim-web-devicons' },
	cmd = { 'Trouble', 'TroubleClose', 'TroubleToggle', 'TroubleRefresh' },
	config = function()
		require('trouble').setup({})
		vim.cmd([[
			highlight TroubleNormal guibg=none
		]])
	end,
}
