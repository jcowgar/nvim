return {
	"lukas-reineke/headlines.nvim",
	dependencies = {
		"nvim-treesitter/nvim-treesitter",
	},
	config = function()
		vim.cmd([[
			" Colors from Modus Operandi theme
		    highlight Headline1 guifg=#0f0f0f guibg=#bfc9ff " bg-blue-intense
			highlight Headline2 guifg=#0f0f0f guibg=#dfa0f0 " bg-magenta-intense
			highlight Headline3 guifg=#0f0f0f guibg=#8adf80 " bg-green-intense
			highlight Headline4 guifg=#0f0f0f guibg=#a1d5f9 " bg-cyan-intense
			highlight Headline5 guifg=#0f0f0f guibg=#f3d000 " bg-yellow-intense
			highlight Headline6 guifg=#0f0f0f guibg=#ff8f88 " bg-red-intense
		]])

		require("headlines").setup({
			markdown = {
				fat_headlines = false,
				headline_highlights = {
					"Headline1",
					"Headline2",
					"Headline3",
					"Headline4",
					"Headline5",
					"Headline6",
				}
			}
		})
	end,
}
