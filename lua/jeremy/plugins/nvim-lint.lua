return {
	'mfussenegger/nvim-lint',
	event = { 'BufReadPre', 'BufNewFile', 'InsertLeave' },
	config = function()
		local lint = require('lint')

		lint.linters_by_ft = {
			ansible = { 'ansible-lint' },
			elixir = { 'credo' },
			go = { 'golangci-lint', 'revive' },
			javascript = { 'eslint_d' },
			json = { 'jsonlint' },
			lua = { 'luacheck' },
			--markdown = { 'vale' },
			--org = { 'vale' },
			typescript = { 'eslint_d' },
			yaml = { 'yamllint' },
		}

		local lint_augroup = vim.api.nvim_create_augroup('lint', { clear = true })

		vim.api.nvim_create_autocmd({ 'BufEnter', 'BufWritePost', 'InsertLeave' }, {
			group = lint_augroup,
			callback = function()
				lint.try_lint()
			end,
		})
	end,
}
