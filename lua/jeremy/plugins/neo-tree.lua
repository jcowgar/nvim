return {
	'nvim-neo-tree/neo-tree.nvim',
	dependencies = {
		'nvim-lua/plenary.nvim',
		'nvim-tree/nvim-web-devicons',
		'MunifTanjim/nui.nvim',
	},
	cmd = 'Neotree',
	config = function()
		require('neo-tree').setup({
			sources = { 'filesystem', 'buffers', 'git_status', 'document_symbols' },
			filesystem = {
				bind_to_cwd = false,
				follow_current_file = { enabled = true },
				use_libuv_file_watcher = true,
				update_focused_file = {
					enabled = true,
				},
			},
			window = {
				position = 'right',
			},
		})
	end,
	keys = {
		{ '<leader>e', '<cmd>Neotree<cr>', desc = 'Explorer' },
	},
}
