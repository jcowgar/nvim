return {
	{
		"miikanissi/modus-themes.nvim",
		priority = 1000,
		lazy = false,
		config = function()
			require("modus-themes").setup({
				style = "modus_operandi",
				variant = "modus_vivendi",
				transparent = true,
				styles = {
					functions = { bold = false },
				},
			})
			vim.opt.background = 'light'

			vim.cmd([[
				colorscheme modus

				highlight LineNr guifg=#c0c0c0 guibg=none
				highlight CursorLineNr guibg=none

				" Flash plugin (folke/flash)
				highlight FlashLabel guibg=#721045 guifg=#ffffff
				highlight FlashCurrent guibg=#006800 guifg=#ffffff
				highlight FlashMatch guibg=#6f5500 guifg=#ffffff

				" Neo-tree plugin ()
				highlight NeoTreeGitUntracked guibg=#dfa0f0 guifg=#193668
			]])
		end,
	},
}
