return {
	"chrisgrieser/nvim-tinygit",
	ft = { "gitrebase", "gitcommit" }, -- so ftplugins are loaded
	dependencies = {
		"stevearc/dressing.nvim",
		"nvim-telescope/telescope.nvim", -- either telescope or fzf-lua
		-- "ibhagwan/fzf-lua",
		"rcarriga/nvim-notify",    -- optional, but will lack some features without it
	},
	keys = {
		{ "ga",    "<cmd>Gitsigns add_hunk<cr>",                    desc = "Add hunk" },
		{ "<C-c>", function() require("tinygit").smartCommit() end, desc = "Smart commit" },
		{ "gp",    function() require("tinygit").push() end,        desc = "Git push" },
	}
}
