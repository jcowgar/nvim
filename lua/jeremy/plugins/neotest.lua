return {
	{
		'nvim-neotest/neotest',
		dependencies = {
			'jfpedroza/neotest-elixir',
			"nvim-neotest/nvim-nio"
		},
		lazy = false,
		config = function()
			require('neotest').setup({
				adapters = {
					require('neotest-elixir'),
				},
				diagnostic = {
					enabled = true,
					severity = 1,
				},
				status = {
					enabled = true,
					signs = true,
					virtual_text = true,
				},
			})
		end,
		keys = {
			{
				'<leader>cts',
				'<cmd>lua require("neotest").summary.toggle()<cr>',
				desc = 'Test Summary',
			},
			{
				'<leader>ctw',
				'<cmd>lua require("neotest").watch.watch()<cr>',
				desc = 'Test Watch',
			},
		},
	},
	{
		'andythigpen/nvim-coverage',
		dependencies = { 'nvim-lua/plenary.nvim' },
		events = { 'BufEnter' },
		config = function()
			require('coverage').setup({
				auto_reload = true,
				highlights = {
					covered = { fg = '#009900' },
					uncovered = { fg = '#990000' },
				},
				lang = {
					elixir = {
						coverage_file = 'cover/lcov.info',
					},
				},
			})
		end,
	},
}
