return {
	'nvim-lualine/lualine.nvim',
	lazy = false,
	config = function()
		require('lualine').setup({
			sections = {
				lualine_a = { 'mode' },
				lualine_b = { 'branch', 'diff', 'diagnostics' },
				lualine_c = { { 'filename', path = 1, }, },
				lualine_x = {},
				lualine_y = { 'selectioncount' },
				lualine_z = { 'location' },
			},
			options = {
				section_separators = { left = '', right = '' },
				component_separators = { left = '', right = '' }
			}
		})
	end,
}
