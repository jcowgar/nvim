return {
	"jackMort/ChatGPT.nvim",
	event = "VeryLazy",
	config = function()
		require("chatgpt").setup()
	end,
	dependencies = {
		"MunifTanjim/nui.nvim",
		"nvim-lua/plenary.nvim",
		"nvim-telescope/telescope.nvim",
	},
	keys = {
		{ "<leader>Cc", "<cmd>ChatGPT<cr>", desc = "ChatGPT" },
		-- { "<leader>Ct", "<cmd>ChatGPTRun add_tests<cr>", desc = "Add Tests" },
		-- { "<leader>Ce", "<cmd>ChatGPTRun explain_code<cr>", desc = "Explain Code" },
	},
}
