return {
	'sindrets/diffview.nvim',
	requires = { 'nvim-tree/nvim-web-devicons' },
	cmd = {
		'DiffviewOpen',
		'DiffviewClose',
		'DiffviewToggleFiles',
		'DiffviewFocusFiles',
		'DiffviewRefresh',
		'DiffviewFileHistory',
	},
	keys = {
		{ '<leader>gf', '<cmd>DiffviewFileHistory %<cr>', desc = 'File History' },
		{ '<leader>gd', '<cmd>DiffviewOpen<cr>', desc = 'Diff View' },
		{ '<leader>gD', '<cmd>:tabclose<cr>', desc = 'Close Diff View' },
	},
	config = function()
		require('diffview').setup({
			keymaps = {
				view = {
					{ 'n', 'q', ':DiffviewClose<CR>' },
				},
				file_panel = {
					{ 'n', 'q', ':DiffviewClose<CR>' },
				},
				file_history_panel = {
					{ 'n', 'q', ':DiffviewClose<CR>' },
				},
			},
		})
	end,
}
