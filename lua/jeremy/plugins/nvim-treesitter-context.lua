return {
	'nvim-treesitter/nvim-treesitter-context',
	dependencies = { 'nvim-treesitter/nvim-treesitter' },
	event = { 'BufReadPre', 'BufNewFile' },
	cmd = { 'TSContextEnable', 'TSContextDisable', 'TSContextToggle' },
	config = function()
		require('treesitter-context').setup({
			enable = true,
		})
	end,
}
