return {
	'asiryk/auto-hlsearch.nvim',
	event = { 'BufReadPre', 'BufNewFile' },
	config = function()
		require('auto-hlsearch').setup({})
	end,
}
