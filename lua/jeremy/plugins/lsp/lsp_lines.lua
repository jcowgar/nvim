return {
	"https://git.sr.ht/~whynothugo/lsp_lines.nvim",
	event = { 'BufReadPre', 'BufNewFile' },
	config = function()
		require("lsp_lines").setup()

		vim.diagnostic.config({
			virtual_text = false,
			underline = false,
			highlight_whole_line = false,
		})

		vim.keymap.set('n', '<leader>.d', require('lsp_lines').toggle, { desc = 'Toggle virtual lines' })
	end,
}
