return {
	{ 
		'williamboman/mason.nvim',
		dependencies = {
			'williamboman/mason-lspconfig.nvim',
		},
		cmd = 'Mason',
		keys = {
			{ '<leader>.m', '<cmd>Mason<cr>', desc = 'Mason' },
		},
		event = { 'BufReadPre', 'BufNewFile' },
		build = ':MasonUpdate',
		config = function(_, opts)
			local mason = require('mason')
			local mason_lspconfig = require('mason-lspconfig')

			mason.setup({

			})

			mason_lspconfig.setup({
				-- these are ONLY LSP servers, not formatters or linters
				ensure_installed = {
					--'ansible-lint',
					'ansiblels',
					'bashls',
					'dockerls',
					--'eslint_d',
					'elixirls',
					'gopls',
					--'gofumpt',
					--'golangci-lint',
					'jsonls',
					'jqls',
					--'luacheck',
					'lua_ls',
					'marksman',
					--'prettierd',
					--'stylua',
					--'shfmt',
					'tsserver',
					'tailwindcss',
					'yamlls',
					--'yamlfmt',
					--'yamllint',
					'zls'
				},
				automatic_installation = true, -- install not listed LSP on first use
			})
		end
	},
}
