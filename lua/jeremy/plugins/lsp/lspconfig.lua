return {
	'neovim/nvim-lspconfig',
	event = { 'BufReadPre', 'BufNewFile' },
	dependencies = {
		'hrsh7th/cmp-nvim-lsp',
		-- notify lsp servers of file renames that occur in neotree
		{ 'antosha417/nvim-lsp-file-operations', config = true },
	},
	keys = {
		{ '<leader>.L', '<cmd>LspInfo<cr>', desc = 'LSP Info' },
	},
	config = function()
		local signs = { Error = '💣', Warn = '', Hint = '💡', Info = ' ' }
		for type, icon in pairs(signs) do
			local hl = 'DiagnosticSign' .. type
			vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = '' })
		end

		local lspconfig = require('lspconfig')
		local cmp_nvim_lsp = require('cmp_nvim_lsp')

		local keymap_opts = { noremap = true, silent = true }
		local keymap = vim.keymap

		local on_attach = function(_, bufnr)
			keymap_opts.buffer = bufnr

			keymap_opts.desc = 'LSP References'
			keymap.set('n', 'gR', '<cmd>Telescope lsp_references<cr>', keymap_opts)

			keymap_opts.desc = 'Goto definition'
			keymap.set('n', 'gd', vim.lsp.buf.definition, keymap_opts)

			keymap_opts.desc = 'Show LSP references'
			keymap.set('n', 'gD', '<cmd>Telescope lsp_references<cr>', keymap_opts)

			keymap_opts.desc = 'Show LSP implementations'
			keymap.set('n', 'gi', '<cmd>Telescope lsp_implementations<cr>', keymap_opts)

			keymap_opts.desc = 'Show LSP type definitions'
			keymap.set('n', 'gt', '<cmd>Telescope lsp_type_definitions<cr>', keymap_opts)

			keymap_opts.desc = 'Show document symbols'
			keymap.set('n', 'gs', '<cmd>Telescope lsp_document_symbols symbol_width=75<cr>', keymap_opts)

			-- conflicts with ga = add hunk, Elixir (primary language) doesn't support
			-- code actions yet anyway. Need to rethink the key conflict though.
			-- keymap_opts.desc = 'Code Actions'
			-- keymap.set({ 'n', 'v' }, 'ga', vim.lsp.buf.code_action, keymap_opts)

			keymap_opts.desc = 'Rename'
			keymap.set('n', '<leader>cr', vim.lsp.buf.rename, keymap_opts)

			keymap_opts.desc = 'Buffer Diagnostics'
			keymap.set(
				'n',
				'<leader>cX',
				'<cmd>Telescope diagnostics bufnr=0<cr>',
				keymap_opts
			)

			keymap_opts.desc = 'Line Diagnostics'
			keymap.set('n', '<leader>cx', vim.diagnostic.open_float, keymap_opts)

			keymap_opts.desc = 'Previous Diagnostic'
			keymap.set('n', '[d', vim.diagnostic.goto_prev, keymap_opts)

			keymap_opts.desc = 'Next Diagnostic'
			keymap.set('n', ']d', vim.diagnostic.goto_next, keymap_opts)

			keymap_opts.desc = 'Documentation'
			keymap.set('n', 'gk', vim.lsp.buf.hover, keymap_opts)

			keymap_opts.desc = 'Restart LSP'
			keymap.set('n', '<leader>.R', '<cmd>LspRestart<cr>', keymap_opts)
		end

		local capabilities = cmp_nvim_lsp.default_capabilities()

		lspconfig['lua_ls'].setup({
			capabilities = capabilities,
			on_attach = on_attach,
			settings = {
				Lua = {
					runtime = {
						version = 'LuaJIT',
					},
					diagnostics = {
						globals = {
							'vim',
							'require',
						},
					},
					workspace = {
						library = {
							--vim.api.nvim_get_runtime_file('', true)
							[vim.fn.expand('$VIMRUNTIME/lua')] = true,
							[vim.fn.stdpath('config') .. '/lua'] = true,
						},
					},
					telemetry = {
						enable = false,
					},
				},
			},
		})

		lspconfig['tsserver'].setup({
			capabilities = capabilities,
			on_attach = on_attach,
		})

		lspconfig['tailwindcss'].setup({
			capabilities = capabilities,
			on_attach = on_attach,
		})

		lspconfig['elixirls'].setup({
			capabilities = capabilities,
			on_attach = on_attach,
			cmd = {
				vim.fn.expand('$HOME/Projects/other/elixir-ls/scripts/language_server.sh'),
			},
			cmd_env = {
				ELS_LOCAL = 1,
			},
			settings = {
				elixirLS = {
					dialyzerEnabled = true,
					incrementalDialyzer = true,
					suggestSpecs = true,
					enableTestLenses = true,
					signatureAfterComplete = true,
				},
			},
		})
	end,
}
