return {
	'onsails/lspkind.nvim',
	dependencies = { 'hrsh7th/nvim-cmp' },
	event = { 'BufReadPre', 'BufNewFile' },
	config = function()
		local cmp = require('cmp')
		local lspkind = require('lspkind')

		cmp.setup({
			formatting = {
				--fields = { "kind", "abbr", "menu" },
				format = lspkind.cmp_format({
					mode = 'symbol_text',
				}),
			},
		})
	end,
}
