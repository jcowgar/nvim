return {
	'mickael-menu/zk-nvim',
	keys = {
		{ '<leader>zb', '<cmd>ZkBacklinks<cr>',        desc = 'Backlinks' },
		{ '<leader>zl', '<cmd>ZkNotes<cr>',            desc = 'List Notes' },
		{ '<leader>zn', '<cmd>ZkNew<cr>',              desc = 'New Note' },
		{ '<leader>zr', '<cmd>ZkIndex<cr>',            desc = 'Reindex' },
		{ '<leader>zt', '<cmd>ZkTags<cr>',             desc = 'List Tags' },
		{ '<leader>zj', '<cmd>ZkJournal<cr>',          desc = 'List Journal Entries' },
		{ '<leader>zN', '<cmd>ZkJournalNew<cr>',       desc = 'New Journal Entry' },
		{ '<leader>za', '<cmd>ZkAmpsMeetingNotes<cr>', desc = 'List AMPS Meeting Notes' },
		{ '<leader>zA', '<cmd>ZkAmpsMeetingNew<cr>',   desc = 'New AMPS Meeting Note' },
	},
	config = function()
		local zk = require('zk')

		zk.setup({
			picker = 'telescope',
			lsp = {
				config = {
					cmd = { 'zk', 'lsp' },
					name = 'zk',
				},
			},
			auto_attach = {
				enabled = true,
				filetypes = { 'markdown' },
			},
		})

		vim.api.nvim_set_keymap(
			'v',
			'<leader>zf',
			":'<,'>ZkMatch<cr>",
			{ noremap = true, silent = false, desc = 'Find selected text' }
		)
		vim.api.nvim_set_keymap(
			'v',
			'<leader>zn',
			":'<,'>ZkNewFromContentSelection { title = vim.fn.input('Title: ') }<CR>",
			{ noremap = true, silent = false, desc = 'New note from selected text' }
		)

		local commands = require('zk.commands')

		commands.add('ZkJournal', function(options)
			options = vim.tbl_extend(
				'force',
				{ hrefs = { 'journal/daily' }, sort = { 'path-' } },
				options or {}
			)
			zk.edit(options, { title = 'Journal Notes' })
		end)

		commands.add('ZkJournalNew', function(options)
			options = vim.tbl_extend('force', { dir = 'journal/daily' }, options or {})
			zk.new(options)
		end)

		commands.add('ZkAmpsMeetingNotes', function(options)
			options = vim.tbl_extend(
				'force',
				{ hrefs = { 'amps/meeting' }, sort = { 'path-' } },
				options or {}
			)
			zk.edit(options, { title = 'AMPS Meeting Notes' })
		end)

		commands.add('ZkAmpsMeetingNew', function(options)
			options = vim.tbl_extend('force', { dir = 'amps/meeting' }, options or {})
			zk.new(options)
		end)
	end,
}
