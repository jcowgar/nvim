return {
	'NeogitOrg/neogit',
	dependencies = {
		'nvim-lua/plenary.nvim',
		'nvim-telescope/telescope.nvim',
		'sindrets/diffview.nvim',
		'ibhagwan/fzf-lua',
	},
	cmd = 'Neogit',
	config = true,
	keys = {
		{ '<leader>gg', '<cmd>Neogit<cr>', desc = 'Git' },
	}
}
