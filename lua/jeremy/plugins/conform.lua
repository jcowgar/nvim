return {
	'stevearc/conform.nvim',
	event = { 'BufReadPre', 'BufNewFile' },
	config = function()
		local conform = require('conform')

		conform.setup({
			formatters_by_ft = {
				bash = { 'shfmt' },
				go = { 'gofumpt', 'goimports' },
				lua = { 'stylua' },
				javascript = { 'prettierd' },
				json = { 'prettierd' },
				markdown = { 'prettierd' },
				typescript = { 'prettierd' },
				yaml = { 'prettierd' },
			},
			format_on_save = {
				timeout_ms = 500,
				lsp_fallback = true,
			},
		})
	end,
}
