return {
	{
		'nvim-telescope/telescope.nvim',
		depdendencies = {
			'nvim-lua/plenary.nvim',
		},
		config = function()
			require('telescope').setup {
				defaults = {
					layout_strategy = "vertical",
					layout_config = {
						vertical = { width = 0.9 },
					},
					mappings = {
						i = {
							["<C-h>"] = "which_key",
						}
					}
				},
				extensions = {
					fzf = {
						fuzzy = true,
						override_generic_sorter = true,
						override_file_sorter = true,
						case_mode = "smart_case",
					},
					project = {
						base_dirs = {
							{ "~/Projects", max_depth = 3 },
						},
					},
				},
			}

			local builtins = require('telescope.builtin')

			vim.keymap.set('n', '<leader>ff', builtins.find_files, { desc = 'Find File' })
			vim.keymap.set('n', '<leader>fF', builtins.git_files, { desc = 'Find Git File' })
			vim.keymap.set('n', '<leader>fr', builtins.oldfiles, { desc = 'Find Old Files' })
			vim.keymap.set('n', '<leader>fg', builtins.live_grep, { desc = 'Grep Files' })
			vim.keymap.set('n', '<leader>fb', "<cmd>Telescope file_browser path=%:p:h select_buffer=true<cr>",
				{ desc = 'File browser' })
			vim.keymap.set('n', '<leader>fB', "<cmd>Telescope file_browser<cr>", { desc = 'File browser (proj dir)' })
			vim.keymap.set('n', '<leader>fp', "<cmd>Telescope project<cr>", { desc = 'Select project' })
			vim.keymap.set(
				'n',
				'<leader>fl',
				builtins.current_buffer_fuzzy_find,
				{ desc = 'Grep Lines' }
			)
			vim.keymap.set('n', '<leader>b', builtins.buffers, { desc = 'Buffers' })
			vim.keymap.set('n', '<leader>t', builtins.treesitter, { desc = 'Treesitter' })
			vim.keymap.set('n', '<leader>?k', builtins.keymaps, { desc = 'Keymaps' })
			vim.keymap.set('n', '<leader>?t', builtins.help_tags, { desc = 'Vim Help' })
			vim.keymap.set('n', '<leader>?m', builtins.man_pages, { desc = 'Man pages' })
			vim.keymap.set('n', '<leader>?o', builtins.vim_options, { desc = 'Vim Options' })
			vim.keymap.set('n', '<leader>gc', builtins.git_commits, { desc = 'Commits' })
			vim.keymap.set('n', '<leader>gb', builtins.git_bcommits, { desc = 'Buffer Commits' })
			vim.keymap.set('n', '<leader>gs', builtins.git_status, { desc = 'Status' })
		end,
	},
	{
		"nvim-telescope/telescope-ui-select.nvim",
		dependencies = { "nvim-telescope/telescope.nvim" },
		config = function()
			require("telescope").load_extension("ui-select")
		end
	},
	{
		'nvim-telescope/telescope-fzf-native.nvim',
		dependencies = { "nvim-telescope/telescope.nvim" },
		build =
		'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build',
		config = function()
			require('telescope').load_extension('fzf')
		end

	},
	{
		"nvim-telescope/telescope-project.nvim",
		dependencies = { "nvim-telescope/telescope.nvim", "nvim-telescope/telescope-file-browser.nvim" },
		config = function()
			require("telescope").load_extension("project")
		end
	},
	{
		"nvim-telescope/telescope-file-browser.nvim",
		dependencies = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" },
		config = function()
			require("telescope").load_extension("file_browser")
		end
	},
	{
		"axkirillov/easypick.nvim",
		dependencies = {
			"nvim-telescope/telescope.nvim"
		},
		config = function()
			local easypick = require("easypick")

			-- only required for the example to work
			local base_branch = "main"

			easypick.setup({
				pickers = {
					-- add your custom pickers here
					-- below you can find some examples of what those can look like

					-- list files inside current folder with default previewer
					{
						-- name for your custom picker, that can be invoked using :Easypick <name> (supports tab completion)
						name = "ls",
						-- the command to execute, output has to be a list of plain text entries
						command = "ls",
						-- specify your custom previwer, or use one of the easypick.previewers
						previewer = easypick.previewers.default()
					},

					-- diff current branch with base_branch and show files that changed with respective diffs in preview
					{
						name = "changed_files",
						command = "git diff --name-only $(git merge-base HEAD " .. base_branch .. " )",
						previewer = easypick.previewers.branch_diff({ base_branch = base_branch })
					},

					-- list files that have conflicts with diffs in preview
					{
						name = "conflicts",
						command = "git diff --name-only --diff-filter=U --relative",
						previewer = easypick.previewers.file_diff()
					},
				}
			})

			vim.keymap.set('n', '<leader>fa', "<cmd>Easypick changed_files<cr>", { desc = "Open a changed file" })
		end,
	},
	{
		'debugloop/telescope-undo.nvim',
		dependencies = { 'nvim-telescope/telescope.nvim' },
		keys = {
			{ '<leader>u', '<cmd>:Telescope undo<cr>', desc = 'Undo Tree' },
		},
	}
}
