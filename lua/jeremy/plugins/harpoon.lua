return {
	{
		'ThePrimeagen/harpoon',
		keys = {
			{
				'<leader>ha',
				function()
					require('harpoon.ui').nav_file(1)
				end,
				desc = 'Goto harpoon file 1',
			},
			{
				'<leader>hr',
				function()
					require('harpoon.ui').nav_file(2)
				end,
				desc = 'Goto harpoon file 2',
			},
			{
				'<leader>hs',
				function()
					require('harpoon.ui').nav_file(3)
				end,
				desc = 'Goto harpoon file 3',
			},
			{
				'<leader>ht',
				function()
					require('harpoon.ui').nav_file(4)
				end,
				desc = 'Goto harpoon file 4',
			},
			{
				'<leader>hg',
				function()
					require('harpoon.mark').add_file()
				end,
				desc = 'Add file to harpoon',
			},
			{
				'<leader>hh',
				function()
					require('harpoon.ui').toggle_quick_menu()
				end,
				desc = 'Show harpoon',
			},
			{
				'<leader>he',
				function()
					require('harpoon.ui').nav_next()
				end,
				desc = 'Goto the next harpooned file',
			},
			{
				'<leader>hu',
				function()
					require('harpoon.ui').nav_prev()
				end,
				desc = 'Goto the previous harpooned file',
			},
		},
		requires = { 'nvim-lua/plenary.nvim' },
		config = function()
			local harpoon = require('harpoon')

			harpoon.setup({})
			require('telescope').load_extension('harpoon')
		end,
	},
}
