return {
	'nvim-treesitter/nvim-treesitter-refactor',
	dependencies = { 'nvim-treesitter/nvim-treesitter' },
	event = { 'BufReadPre', 'BufNewFile' },
	config = function()
		require('nvim-treesitter.configs').setup({
			refactor = {
				highligh_definitions = {
					enable = true,
					clear_on_cursor_move = true,
				},
				highlight_current_scope = {
					-- I found this too distracting
					enable = false
				},
				smart_rename = {
					enable = true,
					keymaps = {
						smart_rename = 'grr',
					},
				},
			},
		})

		local context = require('treesitter-context')

		vim.keymap.set('n', '[c', context.go_to_context, { desc = 'Goto Context' })
	end,
}
