return {
	'tzachar/highlight-undo.nvim',
	opts = {},
	config = function()
		require("highlight-undo").setup({

			duration = 300,
			undo = {
				higroup = "HighlightUndo",
				mode = "n",
				lhs = "z",
				map = 'undo',
				opts = {}
			},
			redo = {
				higroup = "HighlightUndo",
				mode = "n",
				lhs = "Z",
				map = 'redo',
				opts = {}
			}
		})
	end
}
