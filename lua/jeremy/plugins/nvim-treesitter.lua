return {
	'nvim-treesitter/nvim-treesitter',
	build = ':TSUpdate',
	event = { 'BufReadPre', 'BufNewFile' },
	config = function()
		require('nvim-treesitter.configs').setup({
			ensure_installed = {
				'elixir',
				'heex',
				'javascript',
				'lua',
				'markdown',
				'markdown_inline',
				'org',
				'regex',
				'toml',
				'typescript',
				'vim',
				'zig',
			},
			auto_install = true,
			highlight = {
				enable = true,
			},
		})
	end,
}
