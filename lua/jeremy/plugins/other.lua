return {
	{
		"rgroli/other.nvim",
		cmd = {
			"Other",
			"OtherSplit",
			"OtherVSplit",
			"OtherClear",
		},
		config = function()
			require("other-nvim").setup({
				mappings = {
					{
						context = "test",
						pattern = "/lib/(.*)/(.*).ex",
						target = "/test/%1/%2_test.exs",
					},
					{
						context = "source",
						pattern = "/test/(.*)/(.*)_test.exs",
						target = "/lib/%1/%2.ex",
					},
				},
			})
		end,
		keys = {
			{ "<leader>co", "<cmd>Other<cr>", desc = "Other File" },
			{ "<leader>c-", "<cmd>OtherSplit<cr>", desc = "Other File Split" },
			{ "<leader>c|", "<cmd>OtherVSplit<cr>", desc = "Other File VSplit" },
		},
	},
}

