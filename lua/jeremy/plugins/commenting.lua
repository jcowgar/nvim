return {
	{
		-- set commentstring based on the surrounding context. In an
		-- Elixir file, the commentstring may be # but what if you are
		-- inside a ~H sigel?

		'JoosepAlviste/nvim-ts-context-commentstring',
		dependencies = { 'nvim-treesitter' },
		event = { 'BufReadPre', 'BufNewFile' },
	},
	{
		'numToStr/Comment.nvim',
		event = { 'BufReadPre', 'BufNewFile' },
		config = true,
	},
}
