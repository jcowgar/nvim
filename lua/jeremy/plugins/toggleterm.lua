return {
	'akinsho/toggleterm.nvim',
	version = '*',
	config = function()
		require('toggleterm').setup({
			open_mapping = [[<c-\>]],
			size = function(term)
				if term.direction == 'horizontal' then
					return 15
				elseif term.direction == 'vertical' then
					return vim.o.columns * 0.4
				else
					return 20
				end
			end,
			direction = 'float',
		})

		local Terminal = require('toggleterm.terminal').Terminal

		local mix_test = Terminal:new({
			cmd = 'mix test.watch',
			hidden = true,
			direction = 'horizontal',
			name = 'Mix Test',
			start_in_insert = false,
			auto_scroll = true,
		})

		function _mix_test_toggle()
			mix_test:toggle()
		end

		vim.api.nvim_set_keymap(
			'n',
			'<leader>cet',
			'<cmd>lua _mix_test_toggle()<cr>',
			{ noremap = true, silent = true }
		)

		vim.api.nvim_set_keymap(
			'n',
			'<c-t>',
			'<cmd>lua _mix_test_toggle()<cr>',
			{ noremap = true, silent = true, desc = 'Toggle Mix Test' }
		)

		vim.cmd([[
			autocmd TermEnter term://*toggleterm#*
				\ tnoremap <silent><c-t> <cmd>lua _mix_test_toggle()<cr>
		]])

		function _G.set_terminal_keymaps()
			local opts = { buffer = 0 }
			vim.keymap.set('t', '<esc>', [[<C-\><C-n>]], opts)
			--vim.keymap.set('t', 'jk', [[<C-\><C-n>]], opts)
			vim.keymap.set('t', '<C-h>', [[<Cmd>wincmd h<CR>]], opts)
			vim.keymap.set('t', '<C-j>', [[<Cmd>wincmd j<CR>]], opts)
			vim.keymap.set('t', '<C-k>', [[<Cmd>wincmd k<CR>]], opts)
			vim.keymap.set('t', '<C-l>', [[<Cmd>wincmd l<CR>]], opts)
			vim.keymap.set('t', '<C-w>', [[<C-\><C-n><C-w>]], opts)
			vim.keymap.set('n', 'gf', [[<<C-w><C-k>]], opts)
		end

		-- if you only want these mappings for toggle term use term://*toggleterm#* instead
		vim.cmd('autocmd! TermOpen term://* lua set_terminal_keymaps()')
	end,
}
