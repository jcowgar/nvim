vim.g.mapleader = ' '
-- Set the terminal title for the buffer/dir I am in.
vim.opt.title = true

vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = false
vim.opt.autoindent = true
vim.opt.smartindent = true

vim.opt.termguicolors = true
vim.opt.background = 'light'

vim.opt.showmode = false

--
-- Line Numbering and Gutter
--
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.numberwidth = 3
vim.opt.signcolumn = 'yes'

-- Do not display ~ on lines end of buffer
vim.wo.fillchars = 'eob: '

--
-- Scrolling
--
vim.opt.scrolloff = 4

--
-- Yet to be categorized
--
vim.opt.cursorline = true
vim.opt.timeoutlen = 550
vim.opt.updatetime = 50
vim.opt.undofile = true

vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
