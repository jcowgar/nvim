local wk = require('which-key')
wk.register({
	['<leader>'] = {
		['.'] = {
			name = 'Config',
			['l'] = { name = 'Line' },
		},
		['c'] = { name = 'Coding' },
		['f'] = { name = 'File' },
		['g'] = {
			name = 'Git',
			['h'] = { name = 'Hunk' },
		},
		['h'] = { name = 'Harpoon' },
		['o'] = { name = 'Orgmode' },
		['q'] = { name = 'Quit' },
		['w'] = { name = 'Window' },
		['z'] = { name = 'Zettlekasten' },
	},
})

vim.keymap.set('n', '<leader>.z', '<cmd>Lazy<cr>', { desc = 'Lazy Neovim' })

function LineNumbersOff()
	vim.o.number = false
	vim.o.relativenumber = false
end

function LineNumbersRelative()
	vim.o.number = true
	vim.o.relativenumber = true
end

function LineNumbersNormal()
	vim.o.number = true
	vim.o.relativenumber = false
end

vim.keymap.set(
	'n',
	'<leader>.lr',
	'<cmd>:lua LineNumbersRelative()<cr>',
	{ desc = 'Relative' }
)
vim.keymap.set(
	'n',
	'<leader>.ln',
	'<cmd>:lua LineNumbersNormal()<cr>',
	{ desc = 'Normal' }
)
vim.keymap.set(
	'n',
	'<leader>.lo',
	'<cmd>:lua LineNumbersOff()<cr>',
	{ desc = 'Off' }
)

vim.keymap.set('n', '[v', '<cmd>:lua require("coverage").jump_prev("uncovered")<cr>',
	{ desc = 'Previous uncovered' })
vim.keymap.set('n', ']v', '<cmd>:lua require("coverage").jump_next("uncovered")<cr>', { desc = 'Next uncovered' })
vim.keymap.set('n', '[q', '<cmd>cp<cr>', { desc = "Previous quick list" })
vim.keymap.set('n', ']q', '<cmd>cn<cr>', { desc = "Next quick list" })
vim.keymap.set('n', '<leader>ctc', '<cmd>:CoverageToggle<cr>', { desc = 'Toggle Code Coverage' })
vim.keymap.set('n', '<leader>ctc', '<cmd>:CoverageToggle<cr>', { desc = 'Toggle Code Coverage' })
vim.keymap.set('n', '<leader>ctC', '<cmd>:CoverageLoad<cr>', { desc = 'Load Code Coverage' })
vim.keymap.set('n', '<leader>cts', '<cmd>:CoverageSummary<cr>', { desc = 'Coverage Summary' })

vim.keymap.set('n', '<leader>d', '<cmd>DBUI<cr>', { desc = 'Database Access' })

vim.keymap.set('n', '<leader>wo', '<cmd>only<cr>', { desc = 'Close Others' })
vim.keymap.set('n', '<leader>wv', '<cmd>vsplit<cr>', { desc = 'Vertical Split' })
vim.keymap.set('n', '<leader>wh', '<cmd>split<cr>', { desc = 'Horizontal Split' })
vim.keymap.set('n', '<leader><leader>', '<cmd>w<cr>', { desc = 'Save' })
vim.keymap.set('n', '<leader>x', '<cmd>TroubleToggle<cr>', { desc = 'Diagnostics' })
vim.keymap.set('n', '<leader>fc', '<cmd>:w|bp|bd #<cr>', { desc = 'Close File Saving' })
vim.keymap.set('n', '<leader>fC', '<cmd>:bp|bd! #<cr>', { desc = 'Close File NO Save' })
vim.keymap.set('n', '<leader>fo', '<cmd>:Oil<cr>', { desc = 'Oil' })
vim.keymap.set('n', '<leader>fs', '<cmd>:w<cr>', { desc = 'Save File' })
vim.keymap.set('n', '<leader>qq', '<cmd>:wqa<cr>', { desc = 'Quit and Save All' })
vim.keymap.set('n', '<leader>qQ', '<cmd>:qa!<cr>', { desc = 'Quit with NO Save ' })

vim.keymap.set({ 'n', 'v' }, '<a-n>', '<c-w>h', { desc = 'Window Left' })
vim.keymap.set({ 'n', 'v' }, '<a-e>', '<c-w>j', { desc = 'Window Down' })
vim.keymap.set({ 'n', 'v' }, '<a-u>', '<c-w>k', { desc = 'Window Up' })
vim.keymap.set({ 'n', 'v' }, '<a-i>', '<c-w>l', { desc = 'Window Right' })

-- keep cursor in middle when cycling through search matches
vim.keymap.set('n', 'n', 'nzzzv', { desc = 'Next Match' })
vim.keymap.set('n', 'N', 'Nzzzv', { desc = 'Prev Match' })

-- keep cursor in middle when moving up/down 1/2 pages
vim.keymap.set('n', '<c-e>', '<c-d>zz', { desc = 'Down 1/2 Page' })
vim.keymap.set('n', '<c-u>', '<c-u>zz', { desc = 'Up 1/2 Page' })

-- tab/shift tab for buffer switching
vim.keymap.set('n', '<tab>', ':bn<cr>', { desc = 'Next buffer', silent = true })
vim.keymap.set('n', '<s-tab>', ':bp<cr>', { desc = 'Previous buffer', silent = true })

-- Colemak Remapping

local colemak = {}

local up, right, down, left

if vim.fn.hostname() == "jdesk" then
	up = 'u'
	right = 'i'
	down = 'e'
	left = 'n'
else
	up = 'i'
	right = 'l'
	down = 'k'
	left = 'j'
end

local mappings = {
	-- Up/down/left/right
	{ modes = { "n", "o", "x" }, lhs = left,     rhs = "h", desc = "Left (h)" },
	{ modes = { "n", "o", "x" }, lhs = up,       rhs = "k", desc = "Up (k)" },
	{ modes = { "n", "o", "x" }, lhs = down,     rhs = "j", desc = "Down (j)" },
	{ modes = { "n", "o", "x" }, lhs = right,    rhs = "l", desc = "Right (l)" },

	-- Beginning/end of line
	-- { modes = { "n", "o", "x" }, lhs = "L",          rhs = "^",          desc = "First non-blank character" },
	-- { modes = { "n", "o", "x" }, lhs = "Y",          rhs = "$",          desc = "End of line" },
	--
	-- -- PageUp/PageDown
	-- { modes = { "n", "x" },      lhs = "j",          rhs = "<PageUp>",   desc = "DESC" },
	-- { modes = { "n", "x" },      lhs = "h",          rhs = "<PageDown>", desc = "DESC" },
	--
	-- -- Jumplist navigation
	-- { modes = { "n" },           lhs = "<C-u>",      rhs = "<C-i>",      desc = "Jumplist forward" },
	-- { modes = { "n" },           lhs = "<C-e>",      rhs = "<C-o>",      desc = "Jumplist forward" },

	-- Word left/right
	{ modes = { "n", "o", "x" }, lhs = "N",      rhs = "b", desc = "Word back" },
	{ modes = { "n", "o", "x" }, lhs = "I",      rhs = "w", desc = "Word forward" },
	{ modes = { "n", "o", "v" }, lhs = "<C-n>",  rhs = "B", desc = "WORD back" },
	{ modes = { "n", "o", "v" }, lhs = "<C-i>",  rhs = "W", desc = "WORD forward" },

	-- -- End of word left/right
	-- { modes = { "n", "o", "x" }, lhs = "N",          rhs = "ge",         desc = "End of word back" },
	-- { modes = { "n", "o", "x" }, lhs = "<M-n>",      rhs = "gE",         desc = "End of WORD back" },
	-- { modes = { "n", "o", "x" }, lhs = "I",          rhs = "e",          desc = "End of word forward" },
	-- { modes = { "n", "o", "x" }, lhs = "<M-i>",      rhs = "E",          desc = "End of WORD forward" },

	-- Text objects
	-- diw is drw. daw is now dsw.
	{ modes = { "o", "v" },      lhs = "r",      rhs = "i", desc = "O/V mode: inner (i)" },
	{ modes = { "o", "v" },      lhs = "s",      rhs = "a", desc = "O/V mode: a/an (a)" },
	-- -- Move visual replace from 'r' to 'R'
	-- { modes = { "o", "v" },      lhs = "R",          rhs = "r",          desc = "Replace" },

	-- -- Folds
	-- { modes = { "n", "x" },      lhs = "b",          rhs = "z" },
	-- { modes = { "n", "x" },      lhs = "bb",         rhs = "zb",         desc = "Scroll line and cursor to bottom" },
	-- { modes = { "n", "x" },      lhs = "bu",         rhs = "zk",         desc = "Move up to fold" },
	-- { modes = { "n", "x" },      lhs = "be",         rhs = "zj",         desc = "Move down to fold" },

	-- -- Copy/paste
	-- { modes = { "n", "o", "x" }, lhs = "c",          rhs = "y" },
	-- { modes = { "n", "x" },      lhs = "v",          rhs = "p" },
	-- { modes = { "n" },           lhs = "C",          rhs = "y$" },
	-- { modes = { "x" },           lhs = "C",          rhs = "y" },
	-- { modes = { "n", "x" },      lhs = "V",          rhs = "P" },

	-- Undo/redo
	-- { modes = { "n" },           lhs = "z",      rhs = "u" },
	-- { modes = { "n" },           lhs = "gz",     rhs = "U" },
	-- { modes = { "n" },           lhs = "Z",      rhs = "<C-r>" },

	-- inSert/append (T)
	-- { modes = { "n" },           lhs = "s",          rhs = "i" },
	-- { modes = { "n" },           lhs = "S",          rhs = "I" },
	-- { modes = { "n" },           lhs = "t",          rhs = "a" },
	-- { modes = { "n" },           lhs = "T",          rhs = "A" },
	{ modes = { "n" },           lhs = "<cr>",   rhs = "i" },
	{ modes = { "n" },           lhs = "<M-cr>", rhs = "I" },
	{ modes = { "n", "v" },      lhs = "<C-a>",  rhs = "I" },

	-- -- Change
	-- { modes = { "n", "o", "x" }, lhs = "w",          rhs = "c" },
	-- { modes = { "n", "x" },      lhs = "W",          rhs = "C" },
	--
	-- -- Visual mode
	-- { modes = { "n", "x" },      lhs = "a",          rhs = "v" },
	-- { modes = { "n", "x" },      lhs = "A",          rhs = "V" },
	--
	-- -- Insert in Visual mode
	-- { modes = { "v" },           lhs = "S",          rhs = "I" },

	-- Search
	{ modes = { "n", "o", "x" }, lhs = "k",      rhs = "n" },
	{ modes = { "n", "o", "x" }, lhs = "K",      rhs = "N" },

	-- -- 'til
	-- -- Breaks diffput
	-- { modes = { "n", "o", "x" }, lhs = "p",          rhs = "t" },
	-- { modes = { "n", "o", "x" }, lhs = "P",          rhs = "T" },
	--
	-- -- Fix diffput (t for 'transfer')
	-- { modes = { "n" },           lhs = "dt",         rhs = "dp",         desc = "diffput (t for 'transfer')" },
	--
	-- -- Macros (replay the macro recorded by qq)
	-- { modes = { "n" },           lhs = "Q",          rhs = "@q",         desc = "replay the 'q' macro" },
	--
	-- -- Cursor to bottom of screen
	-- -- H and M haven't been remapped, only L needs to be mapped
	-- { modes = { "n" },           lhs = "B",          rhs = "L" },
	-- { modes = { "v" },           lhs = "B",          rhs = "L" },
	--
	-- -- Misc overridden keys must be prefixed with g
	-- { modes = { "n", "x" },      lhs = "gX",         rhs = "X" },
	-- { modes = { "n", "x" },      lhs = "gU",         rhs = "U" },
	-- { modes = { "n", "x" },      lhs = "gQ",         rhs = "Q" },
	-- { modes = { "n", "x" },      lhs = "gK",         rhs = "K" },
	-- -- extra alias
	-- { modes = { "n" },           lhs = "gh",         rhs = "K" },
	-- { modes = { "x" },           lhs = "gh",         rhs = "K" },
	--
	-- -- Window navigation
	-- { modes = { "n" },           lhs = "<C-w>n",     rhs = "<C-w>h" },
	-- { modes = { "n" },           lhs = "<C-w>u",     rhs = "<C-w>k" },
	-- { modes = { "n" },           lhs = "<C-w>e",     rhs = "<C-w>j" },
	-- { modes = { "n" },           lhs = "<C-w>i",     rhs = "<C-w>l" },
	-- { modes = { "n" },           lhs = "<C-w>N",     rhs = "<C-w>H" },
	-- { modes = { "n" },           lhs = "<C-w>U",     rhs = "<C-w>K" },
	-- { modes = { "n" },           lhs = "<C-w>E",     rhs = "<C-w>J" },
	-- { modes = { "n" },           lhs = "<C-w>I",     rhs = "<C-w>L" },
	-- -- Disable spawning empty buffer
	-- { modes = { "n" },           lhs = "<C-w><C-n>", rhs = "<nop>" },
}

function colemak.setup(_)
	colemak.apply()

	vim.api.nvim_create_user_command(
		"ColemakEnable",
		colemak.apply,
		{ desc = "Applies Colemak mappings" }
	)
	vim.api.nvim_create_user_command(
		"ColemakDisable",
		colemak.unapply,
		{ desc = "Removes Colemak mappings" }
	)
end

function colemak.apply()
	for _, mapping in pairs(mappings) do
		vim.keymap.set(
			mapping.modes,
			mapping.lhs,
			mapping.rhs,
			{ desc = mapping.desc }
		)
	end
end

function colemak.unapply()
	for _, mapping in pairs(mappings) do
		vim.keymap.del(mapping.modes, mapping.lhs)
	end
end

colemak.apply()
colemak.setup()
