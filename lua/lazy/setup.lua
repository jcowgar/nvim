require('lazy').setup({
	{ import = 'jeremy.plugins' },
	{ import = 'jeremy.plugins.lsp' },
}, {
	install = {
		colorscheme = { 'modus' },
	},
	checker = {
		enabled = true,
		notify = false,
	},
	change_detection = {
		notify = false,
	},
})
