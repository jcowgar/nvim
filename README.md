# Jeremy's NeoVim

I have switched to LazyVim about 8 months ago and really enjoy the lazy.nvim
style of package management. I too have enjoyed the LazyVim configuration
framework. However... I would like to better control what packages I have,
what key bindings are set and understand my setup more fully. Therefore,
I have started my own personal configuration.

This is not a configuration framework for someone else. It is only my
personal setup for me. Maybe it will work for you, but that is not its
intent.
